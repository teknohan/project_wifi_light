/* 
 * ESP8266 Webserver
 * Starts the ESP8266 as an access point and provides a web interface to configure WiFi credentials and control GPIO pins
 * Go to http://192.168.4.1 in a web browser connected to this access point to see it
 * 
 * Created by: Gokhan koc 
 * E-mail: gokhankoc90@gmail.com
 
 * Last modified: 08-13, 2016
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <EEPROM.h>
int wifi_mode =0;
String total_read;
int login_key;
String debug_all;
int debug_mod=1; //debug mode is ON
String response_message; 
// Access point credentials
const char *ap_ssid = "ESPap";
const char *ap_password = "thereisnospoon";
String local_ip;
// ESP8266 GPIO pins
#define GPIO2 2
#define GPIO12 12
#define GPIO13 13
#define GPIO14 14
int loop_counter=0;
const char* host = "www.unigreen-led.com";
const int httpPort = 80;

ESP8266WebServer server(80);

// debug_main ###################################################################################################################
void debug_main(String debug_message){
  if(debug_mod==1){
  debug_all +=debug_message;  
}
}
// logout###################################################################################################################
void logout() 
{
   
debug_main("loged out");
debug_main("<br>");
    
    server.sendHeader("Location","/login");
    server.sendHeader("Cache-Control","no-cache");
    server.sendHeader("Set-Cookie","ESPSESSIONID=0");
    server.send(301);
  
}

bool search_found(String haystack,String needle){
int foundpos = -1;
  for (int i = 0; i <= haystack.length() - needle.length(); i++) {
    if (haystack.substring(i,needle.length()+i) == needle) {
      delay(1);
      foundpos = i;
      return true;
      
    }
  }
 return false;
}



// is_authentified###################################################################################################################
bool is_authentified(){
 
  if (server.hasHeader("Cookie")){
    String cookie = server.header("Cookie");
    String cookie_check = "ESPSESSIONID=";
    cookie_check += login_key;
    if (cookie.indexOf(cookie_check) != -1) {
      return true;
    }

  }

   
  return false;
}
// login_check###################################################################################################################
void login_check(){
 if (!is_authentified()){
  debug_main("need to login");
  debug_main("<br>");
    server.sendHeader("Location","/login");
    server.sendHeader("Cache-Control","no-cache");
    server.send(301);
    return;
  }
  
}
// menu_html###################################################################################################################
void menu_html(){
 
  response_message += "<li><a href=\"/\">Home Page </a></li>";
response_message += "<li><a href=\"/wlan_config\">Configure WLAN settings </a></li>";
response_message += "<li><a href=\"/gpio\">Control GPIO pins </a></ul></li>";
response_message += "<li><a href=\"/debug\">Debug page </a></ul></li>";
   if (!is_authentified()){
   
  response_message += "<ul><li><a href=\"/login\">Login </a></li>";
  }
  if (is_authentified()){
  response_message += "<li><a href=\"/logout\">Logout </a></li>";
  }
}









// GET TIME ###################################################################################################################
String get_time() 
{
const char* host = "www.unigreen-led.com";
const int httpPort = 80;
String url = "/nodemcu/time.php";

WiFiClient client;
if (!client.connect(host, httpPort)) {
debug_main("fail to connect");
 }
else
{
client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                   "Host: " + host + "\r\n" + 
                   "Connection: close\r\n\r\n");
delay(500);
total_read="";
  while(client.available()){
        String line = client.readStringUntil('\r');
        total_read +=line;
      }
}
// debug_main PAGE ###################################################################################################################

debug_main(total_read.substring(163,180));

  }


// AP WIFI MOD CHANGE CHECK POINT ###################################################################################################################

void wifi_first_check(){
  response_message = "";
if (WiFi.status() == WL_CONNECTED)
  {
      IPAddress localAddr = WiFi.localIP();
  byte oct1 = localAddr[0];
  byte oct2 = localAddr[1];
  byte oct3 = localAddr[2];
  byte oct4 = localAddr[3];
  char s[16];  
  sprintf(s, "%d.%d.%d.%d", oct1, oct2, oct3, oct4);
String local_link = s;
local_link +="/gpio";
    response_message += "<center>WLAN Status: Connected</center><br><h1>please connect your  computer or phone to your own wifi  h1>";
    response_message += "<center>WLAN Status: Connected</center><br><h1>to control the light localy <a href='";
    response_message +=local_link;
    response_message +="'>click here</a><h1>";
    response_message += "<center>WLAN Status: Connected</center><br><h1>to control the light from remote place <a href='http://www.unigreen-led.com/my_devices'>click here</a><h1>";
    response_message += "</body></html>";
  server.send(200, "text/html", response_message);
ESP.reset();
  }
  else
  {
    response_message += "<center>WLAN Status: Disconnected</center><br> <h1>please re-connect <a href='/wlan_config'>click here</a><h1>";
    response_message += "</body></html>";
  server.send(200, "text/html", response_message);
  }

  
}






// ROOT PAGE ###################################################################################################################
void rootPageHandler() 
{
login_check();
  response_message = "<html><head><title>ESP8266 Webserver</title></head>";
  response_message += "<body style=\"background-color:PaleGoldenRod\"><h1><center>ESP8266 Webserver</center></h1>";
  
  if (WiFi.status() == WL_CONNECTED)
  {
    response_message += "<center>WLAN Status: Connected</center><br>";
  }
  else
  {
    response_message += "<center>WLAN Status: Disconnected</center><br>";
  }
 menu_html();
  response_message += "</body></html>";
  
  server.send(200, "text/html", response_message);
}




// Wlan page for the webserver ###############################################################################################################################*/
void wlanPageHandler()
{
login_check();
  // Check if there are any GET parameters
  if (server.hasArg("ssid"))
  {    
    if (server.hasArg("password"))
    {
      WiFi.begin(server.arg("ssid").c_str(), server.arg("password").c_str());
    }
    else
    {
      WiFi.begin(server.arg("ssid").c_str());
    }
  response_message = "";
  response_message += "<html>";
  response_message += "<head><title>ESP8266 Webserver</title></head>";
  response_message += "<body style=\"background-color:PaleGoldenRod\"><h1><center>Please wait 30 seconds page will redrict</h1>";
  response_message += "<script>setTimeout(function(){window.location.href='/wifi_first_check'},30000);</script></body></html>";
  server.send(200, "text/html", response_message);

    
    int wifi_count=0;
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
debug_main(".");
Serial.print(".");
      if(wifi_count>60){
      EEPROM.write(0,0);
      EEPROM.commit();
      delay(500);
      debug_main("fail to connect wifi");
      Serial.print("fail to connect wifi");
        //ESP.reset();
        return; 
      }
    wifi_count=wifi_count+1;
    }
    Serial.println("connected");
//#################### EEPROM write start
String my_wifi_name  =server.arg("ssid").c_str();
Serial.println(my_wifi_name);
String my_wifi_password = server.arg("password").c_str();
Serial.println(my_wifi_password);
    EEPROM.write(0, 1);
 EEPROM.commit();
delay(50);
 int size_of_passowrd =my_wifi_password.length();
  int size_of_name =my_wifi_name.length();
EEPROM.write(1, size_of_name);
Serial.println(size_of_name);

 EEPROM.commit();
 delay(100);

 EEPROM.write(2, size_of_passowrd);
 Serial.println(size_of_passowrd);
 EEPROM.commit();
 delay(50);

int counter=0;
 for (int i=3; i <= size_of_name+3; i++){

debug_main("<br>");
    
 EEPROM.write(i, my_wifi_name[counter]);
 EEPROM.commit();
 delay(50);
 counter = counter+1;
    }
    counter=0;
    int counter2=0;
debug_main("<br>");
     for (int i=sizeof(my_wifi_name)+3; i <= sizeof(my_wifi_name)+size_of_passowrd+2; i++){
      

 EEPROM.write(i, my_wifi_password[counter2]);
 EEPROM.commit();
 delay(50);
 counter2 =counter2+1;
    }
    counter2=0;


debug_main("<br>");
debug_main("EEPROM 0 : ");
debug_main("<br>");
debug_main(String(EEPROM.read(0)));
debug_main("<br>");
    

//#################### EEPROM write end

      

debug_main("<br>");
debug_main("WiFi connected");  
debug_main("IP address: ");
IPAddress localAddr = WiFi.localIP();
  byte oct1 = localAddr[0];
  byte oct2 = localAddr[1];
  byte oct3 = localAddr[2];
  byte oct4 = localAddr[3];
  char s[16];  
  sprintf(s, "%d.%d.%d.%d", oct1, oct2, oct3, oct4);
debug_main(s);

delay(1000);

  }
  
  response_message = "";
  response_message += "<html>";
  response_message += "<head><title>ESP8266 Webserver</title></head>";
  response_message += "<body style=\"background-color:PaleGoldenRod\"><h1><center>WLAN Settings</center></h1>";
  
  if (WiFi.status() == WL_CONNECTED)
  {
    response_message += "Status: Connected<br>";
  }
  else
  {
    response_message += "Status: Disconnected<br>";
  }
   menu_html();
  response_message += "<p>To connect to a WiFi network, please select a network...</p>";

  // Get number of visible access points
  int ap_count = WiFi.scanNetworks();
  
  if (ap_count == 0)
  {
    response_message += "No access points found.<br>";
  }
  else
  {
    response_message += "<form method=\"get\">";

    // Show access points
    for (uint8_t ap_idx = 0; ap_idx < ap_count; ap_idx++)
    {
      response_message += "<input type=\"radio\" name=\"ssid\" value=\"" + String(WiFi.SSID(ap_idx)) + "\">";
      response_message += String(WiFi.SSID(ap_idx)) + " (RSSI: " + WiFi.RSSI(ap_idx) +")";
      (WiFi.encryptionType(ap_idx) == ENC_TYPE_NONE) ? response_message += " " : response_message += "*";
      response_message += "<br><br>";
    }
    
    response_message += "WiFi password (if required):<br>";
    response_message += "<input type=\"text\" name=\"password\"><br>";
    response_message += "<input type=\"submit\" value=\"Connect\">";
    response_message += "</form>";
  }

  response_message += "</body></html>";
  
  server.send(200, "text/html", response_message);
}
// Login page for the webserver ###############################################################################################################################*/
void login(){


  if (server.hasArg("USERNAME") && server.hasArg("PASSWORD")){
    if (server.arg("USERNAME") == "admin" &&  server.arg("PASSWORD") == "admin" ){
      server.sendHeader("Location","/");
      server.sendHeader("Cache-Control","no-cache");
       String login_cookie = "ESPSESSIONID=";
      login_key = random(9999, 9999999);
     login_cookie += login_key;
      server.sendHeader("Set-Cookie",login_cookie);
String cookie = server.header("Cookie");
debug_main(cookie);
debug_main("<br>");
debug_main("loged in");
debug_main("<br>");
    server.sendHeader("Location","/");
    server.sendHeader("Cache-Control","no-cache");
    server.send(301);
    
    }
else{
//String msg = "Wrong username/password! try again.";
debug_main("Log in Failed");
response_message = "<html><head><title>ESP8266 Webserver</title></head>";
response_message += "<body style=\"background-color:PaleGoldenRod\"><h1><center>Log in Failed</center></h1>";
  response_message += "<li><a href=\"/login\">re-Login</h4></li></ul>";
server.send(200, "text/html", response_message);
}
    
  }
    else{
response_message = "<html><head><title>ESP8266 Webserver</title></head>";
response_message += "<body style=\"background-color:PaleGoldenRod\"><h1><center>Login Page</center></h1>";
response_message += " <form action='/login' method='POST'>";
response_message += " User name:<br>";
response_message += " <input type=\"text\" name=\"USERNAME\" value=\"\"><br>";
response_message += " Password:<br>";
response_message += " <input type=\"password\" name=\"PASSWORD\" value=\"\"><br><br>";
response_message += " <input type=\"submit\" value=\"Submit\">";
response_message +="</form> ";
response_message += "</body></html>";
server.send(200, "text/html", response_message);
    }

}

/* Debug page for the webserver ###############################################################################################################################*/
void debug(){
login_check();

 response_message = "<html><head><title>ESP8266 Webserver debug</title></head> <meta http-equiv='refresh' content='30' />";
  response_message += "<body style=\"background-color:PaleGoldenRod\"><h1><center>Debug page </center></h1>";
   menu_html();
 response_message += "<br>avaliable RAM: ";
    response_message += ESP.getFreeHeap();
   
  response_message += "<center>";
 response_message +=debug_all;
  response_message += "</center></body></html>";

  
  server.send(200, "text/html", response_message);
  
}

// GPIO page allows you to control the GPIO pins ###############################################################################################################################*/
void gpioPageHandler()
{
login_check();
  // Check if there are any GET parameters

    if (server.hasArg("gpio12"))
  { 
    if (server.arg("gpio12") == "1")
    {
      digitalWrite(GPIO12, HIGH);
    }
    if (server.arg("gpio12") == "0")
    {
      digitalWrite(GPIO12, LOW);
    }
  }

  
    if (server.hasArg("gpio13"))
  { 
    if (server.arg("gpio13") == "1")
    {
      digitalWrite(GPIO13, HIGH);
    }
    if (server.arg("gpio13") == "0")
    {
      digitalWrite(GPIO13, LOW);
    }
  }
      if (server.hasArg("gpio14"))
  { 
    if (server.arg("gpio14") == "1")
    {
      digitalWrite(GPIO14, HIGH);
    }
    if (server.arg("gpio14") == "0")
    {
      digitalWrite(GPIO14, LOW);
    }
  }

  response_message = "<html><head><title>ESP8266 Webserver</title></head>";
  response_message += "<body style=\"background-color:PaleGoldenRod\"><h1><center>Control GPIO pins</center></h1>";
  menu_html();
  response_message += "<center><form method=\"get\">";

response_message += "<br>Green<br>";
   if (digitalRead(GPIO12) == LOW)
  {
    response_message += "<input type=\"radio\" name=\"gpio12\" value=\"0\" onclick=\"submit();\" checked>Off<br>";
    response_message += "<input type=\"radio\" name=\"gpio12\" value=\"1\" onclick=\"submit();\" >On<br>";
  }
  else
  {
    response_message += "<input type=\"radio\" name=\"gpio12\" value=\"0\" onclick=\"submit();\" >Off<br>";
    response_message += "<input type=\"radio\" name=\"gpio12\" value=\"1\" onclick=\"submit();\" checked>On<br>";
  }
response_message += "<br>Blue<br>";
   if (digitalRead(GPIO13) == LOW)
  {
    response_message += "<input type=\"radio\" name=\"gpio13\" value=\"0\" onclick=\"submit();\" checked>Off<br>";
    response_message += "<input type=\"radio\" name=\"gpio13\" value=\"1\" onclick=\"submit();\" >On<br>";
  }
  else
  {
    response_message += "<input type=\"radio\" name=\"gpio13\" value=\"0\" onclick=\"submit();\" >Off<br>";
    response_message += "<input type=\"radio\" name=\"gpio13\" value=\"1\" onclick=\"submit();\" checked>On<br>";
  }
response_message += "<br>Red<br>";
   if (digitalRead(GPIO14) == LOW)
  {
    response_message += "<input type=\"radio\" name=\"gpio14\" value=\"0\" onclick=\"submit();\" checked>Off<br>";
    response_message += "<input type=\"radio\" name=\"gpio14\" value=\"1\" onclick=\"submit();\" >On<br>";
  }
  else
  {
    response_message += "<input type=\"radio\" name=\"gpio14\" value=\"0\" onclick=\"submit();\" >Off<br>";
    response_message += "<input type=\"radio\" name=\"gpio14\" value=\"1\" onclick=\"submit();\" checked>On<br>";
  }

  response_message += "</form></center></body></html>";

  
  server.send(200, "text/html", response_message);
}

// Called if requested page is not found ###############################################################################################################################*/
void handleNotFound()
{
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  
  for (uint8_t i = 0; i < server.args(); i++)
  {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  
  server.send(404, "text/plain", message);
}
// pages function ###############################################################################################################################*/
void pages(){

  server.on("/",   rootPageHandler);
  server.on("/wlan_config", wlanPageHandler);
  server.on("/gpio", gpioPageHandler);
  server.on("/login", login);
  server.on("/logout", logout);
  server.on("/debug", debug);
 server.on("/wifi_first_check", wifi_first_check);
  
 server.onNotFound(handleNotFound);
}

// server com loop PAGE ######################################## start ###########################################################################
void com_loop(){
 
   
String url = "/nodemcu/com.php?";
  url += "chip_id=";
  url += ESP.getChipId();
  url += "&local_ip=";
  url += local_ip;
  
 WiFiClient client;


//Serial.println("before connect");

if (!client.connect(host, httpPort)) {
    Serial.println("couldn't connect to client");
}
else
{
  //Serial.println("after connect");

//Serial.println(url);
//Serial.println("before client.print");
client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                   "Host: " + host + "\r\n" + 
                   "Connection: close\r\n\r\n");
                   //Serial.println("after client.print");
delay(50);

total_read="";
delay(1);
Serial.println("---------------");
if(client.available()){
  while(client.available()){
        String line = client.readStringUntil('\r');
        total_read +=line;
  }
  //Serial.print("clean read ");

int start_point = total_read.indexOf("$$");
int second_point = total_read.indexOf("%%");
int third_point = total_read.indexOf("^^");
int fourth_point = total_read.indexOf("&&");
int end_point = total_read.indexOf("**");
String clean_read1 = total_read.substring(start_point+2, second_point);
int clean_read2 = total_read.substring(second_point+2, third_point).toInt();
int clean_read3 = total_read.substring(third_point+2, fourth_point).toInt();
int clean_read4 = total_read.substring(fourth_point+2, end_point).toInt();
//Serial.println("1");
Serial.println(clean_read1);
//Serial.println("2");
Serial.println(clean_read2);
//Serial.println("3");
Serial.println(clean_read3);
//Serial.println("4");
Serial.println(clean_read4);

analogWrite(GPIO14, clean_read2);
analogWrite(GPIO13, clean_read3);
analogWrite(GPIO12,clean_read4);

}else{

Serial.println("server is busy ");  
}

//analogWrite(GPIO14, 255);
//analogWrite(GPIO13, 0);
//analogWrite(GPIO12, 0);
}
 
  
}









// server com loop PAGE ######################################## end ###########################################################################

// SETUP  #################################################### start #################################################################*/
void setup() 
{
  pinMode(GPIO2, OUTPUT);
  pinMode(GPIO12, OUTPUT);
  pinMode(GPIO13, OUTPUT);
  pinMode(GPIO14, OUTPUT);
  EEPROM.begin(512);
  Serial.begin(115200);
  
   Serial.println("serial begin");
  
debug_main("Configuring access point...");
debug_main("<br>");
  
  /* You can add the password parameter if you want the AP to be password protected */
  if(EEPROM.read(0)>0){
    Serial.println("EEPROM = 1");
// ########################################################################################################################*/ ONLINE SETUP
wifi_mode =1;
Serial.println("setup; mode=1");
WiFi.softAPdisconnect(true);
debug_main("online");
debug_main("<br>");




String wifi_name;
for (int i=3; i <= EEPROM.read(1)+2; i++){
char pass_value =EEPROM.read(i);
wifi_name +=String(pass_value);

}
Serial.println(wifi_name);

String wifi_password;
for (int i=3+EEPROM.read(1); i <= EEPROM.read(1)+3+EEPROM.read(2); i++){
char pass_value =EEPROM.read(i);
wifi_password +=String(pass_value);
}
Serial.println(wifi_password);
//debug_main("wifi password: ************");

  //read EEPROM finish
  
char ssid[wifi_name.length()];
wifi_name.toCharArray(ssid, wifi_name.length()+1);
char password[wifi_password.length()];
wifi_password.toCharArray(password, wifi_password.length()+1);
    
debug_main("Connecting to");

//debug_main(ssid);


 
  WiFi.begin(ssid, password);

  int wifi_count=0;
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);

debug_main(".");
Serial.print(".");
      if(wifi_count>60){
      //EEPROM.write(0,0);
      //EEPROM.commit();
 debug_main("fail to connect resetting");
 Serial.println("fail to connect ");
      delay(500);
      wifi_mode =0;
      Serial.println("mode change to 0");
      break;
      }
    wifi_count=wifi_count+1;
    }
    
 if (wifi_mode==1){  
    pages();  /* Set page handler functions */
debug_main("<br>");
debug_main("ONLINE server started");
debug_main("<br>");
debug_main("WiFi connected");
debug_main("<br>");
debug_main("IP address: ");

IPAddress localAddr = WiFi.localIP();
  byte oct1 = localAddr[0];
  byte oct2 = localAddr[1];
  byte oct3 = localAddr[2];
  byte oct4 = localAddr[3];
  char s[16];  
  sprintf(s, "%d.%d.%d.%d", oct1, oct2, oct3, oct4);
debug_main(s);
Serial.println(WiFi.localIP());
local_ip =s; 
debug_main("<br>");

    //here the list of headers to be recorded
  const char * headerkeys[] = {"User-Agent","Cookie"} ;
  size_t headerkeyssize = sizeof(headerkeys)/sizeof(char*);
  //ask server to track these headers
  server.collectHeaders(headerkeys, headerkeyssize );
  server.begin();

delay(2000);

 }
}
// offline mode ###############################################################################################################################*/
 if (wifi_mode==0){
  WiFi.softAP(ap_ssid); // Wifi.softAP(ssid, password)
Serial.println("local mode");
  IPAddress myIP = WiFi.softAPIP();
Serial.println(WiFi.localIP());
debug_main("AP IP address: ");
  IPAddress  localAddr = WiFi.localIP();
  byte oct1 = localAddr[0];
  byte oct2 = localAddr[1];
  byte oct3 = localAddr[2];
  byte oct4 = localAddr[3];
  char s[16];  
  sprintf(s, "%d.%d.%d.%d", oct1, oct2, oct3, oct4);
debug_main(s);

  
  pages(); /* Set page handler functions */

  //here the list of headers to be recorded
  const char * headerkeys[] = {"User-Agent","Cookie"} ;
  size_t headerkeyssize = sizeof(headerkeys)/sizeof(char*);
  //ask server to track these headers
  server.collectHeaders(headerkeys, headerkeyssize );
  server.begin();
  
debug_main("HTTP server started");
    
}
}
// SETUP  #################################################### end #################################################################*/
// LOOP ###############################################################################################################################*/
void loop() 
{
  server.handleClient();
  if(wifi_mode==1){
    int start_millis = millis();
   com_loop(); 
   int current_time =  millis();
int passed_time =current_time-start_millis;

  Serial.println(passed_time);

   loop_counter=loop_counter+1;
   //Serial.println(loop_counter);
 // Serial.println("loop ends");
  }
  
}
